import os
import shutil
import cv2
import time


class Color:
	rgb = [0, 0, 0]
	
	def __init__(self, r, g, b):
		self.rgb[0] = r
		self.rgb[1] = g
		self.rgb[2] = b
		
	def __getitem__(self, i):
		return self.rgb[i]
		
	def __setitem__(self, i, v):
		self.rgb[i] = v
	
	def __str__(self):
		return ','.join(str(v) for v in self.rgb)
		
	def __delitem__(self, v):
		del self.rgb[v]
	

class Cell:
	i = 0
	j = 0
	color = None
	sheet = 0
	r = 0
	g = 0
	b = 0
	
	def __init__(self, i, j, color, sheet):
		self.i = i
		self.j = j
		self.color = color
		self.sheet = sheet

class Sheet:
	index = 0
	cells = []

	def __init__(self, index):
		self.index = index

# Constants
OUTPUT_FILE = 'output.xlsx'
VIDEO_FILE = 'Funny Evangelion Scene.mp4'
FRAME_INC = 10
SCALE = 1
HISTOGRAM_N = 2^8


# Version
print("Using cv2 version {}".format(cv2.__version__))

def create_histogram(n):
	print("histogram: %d" % n)
	MAX_COLOR = 256
	unit = MAX_COLOR / n

	histogram = []
	
	for i in range(1, n + 1):
		histogram.append(i * unit)
		
	return histogram
	
def aprox_color(color, histogram):
	out_color = color
	
	for i, c in enumerate(color):
		for h in histogram:
			if h <= c:
				out_color.rgb[i] = h
			else:
				break
	return out_color


def rgb_to_hex(r, g, b):
	return '%02x%02x%02x' % (int(r), int(g), int(b))
	
#####################
### Process video ###
#####################

print("Processing video...")
start_processing = time.time()

print("Creating histogram %d" % HISTOGRAM_N)
histogram = create_histogram(HISTOGRAM_N)

vidcap = cv2.VideoCapture(VIDEO_FILE)
length = int(vidcap.get(cv2.CAP_PROP_FRAME_COUNT))
count = 0

# Vars
AllSheets = []
sheet_index = 0

while True:
	success, image = vidcap.read()
	if not success:
		break
	
	count += 1
	
	# skip frames
	if count % FRAME_INC != 1 and count != 1:
		continue
	
	print("Frame process: {}/{}".format(count, length))
	
	sheet = Sheet(sheet_index)
	
	cells = []
	x = 0
	y = 0
	step = int(1 / SCALE)
	
	for i in range(0, image.shape[0], step):
		for j in range(0, image.shape[1], step):
			original = Color(int(image[i][j][2]), int(image[i][j][1]), int(image[i][j][0]))
			
			# default color
			r = image[i][j][2]
			g = image[i][j][1]
			b = image[i][j][0]
			
			for k in range(1, step):
				print(k)
				for l in range(1, step):
					ii = i + k
					jj = j + l
					r += int(image[ii][jj][2])
					g += int(image[ii][jj][1])
					b += int(image[ii][jj][0])
			
			r /= step
			g /= step
			b /= step
			col = Color(r, g, b)
			col = aprox_color(col, histogram)
			
			c = Cell(x, y, col, sheet_index)
			c.r = col[0]
			c.g = col[1]
			c.b = col[2]
			
			cells.append(c)
			y += 1
		x += 1
		y = 0
	
	sheet.cells = cells
	AllSheets.append(sheet)
	
	sheet_index += 1

	
# Done analyzing
end_processing = time.time()
print("Done processing video!")



#######################
### Prepare content ###
#######################

print("Preparing content...")
start_preparing = time.time()

import openpyxl
from openpyxl import Workbook
from openpyxl.styles import Color, PatternFill, Font, Border
from openpyxl.styles import colors
from openpyxl.cell import Cell

wb = openpyxl.Workbook()
sheet_count = len(AllSheets)

for i, sheet in enumerate(AllSheets):
	ws = None
	print("Processing sheet %d/%d" % (sheet.index + 1, sheet_count))
	
	if i != 0:
		ws = wb.create_sheet(title="frame{}".format(sheet.index))
	else:
		ws = wb.active
		ws.title = "frame{}".format(sheet.index)

		
	for cell in sheet.cells:
		color = rgb_to_hex(cell.r, cell.g, cell.b)
		cell_fill = PatternFill(start_color=color,
					   end_color=color,
					   fill_type='solid')
		#_ = ws.cell(column=cell.j + 1, row=cell.i + 1, value="%d;%d;%d" % (cell.r, cell.g, cell.b))
		current_cell = ws.cell(column=cell.j + 1, row=cell.i + 1, value="")
		current_cell.fill = cell_fill
		#ws.column_dimensions[cell.column].width = 2
	
	dims = {}
	for row in ws.rows:
		for cell in row:
			# if cell.value == "1":
				# cell.value = ""
			dims[cell.column] = max((dims.get(cell.column, 0), len(str(cell.value))))  
	
	for col, value in dims.items():
		ws.column_dimensions[col].width = 2

# Done preparing content
end_preparing = time.time()
print("Done preparing content!")
		
		
#####################
### Write to file ###
#####################

# Save to file
print("Saving file to %s" % OUTPUT_FILE)
start_write_to_file = time.time()
wb.save(OUTPUT_FILE)

# Done writing to file
end_write_to_file = time.time()
print("Done writing to file!")


###############
### Metrics ###
###############

processing_time = (end_processing - start_processing)
preparing_time = (end_preparing - start_preparing)
writing_to_file_time = (end_write_to_file - start_write_to_file)
total_time = processing_time + preparing_time + writing_to_file_time

print("=== METRICS ===")
print("Processing video: {}".format(processing_time))
print("Preparing content: {}".format(preparing_time))
print("Writing to file: {}".format(writing_to_file_time))
print("Total time spent: {}".format(total_time))