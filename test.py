import os
import shutil
import cv2
import time


class Cell:
	i = 0
	j = 0
	color = ""
	sheet = 0
	
	def __init__(self, i, j, color, sheet):
		self.i = i
		self.j = j
		self.color = color
		self.sheet = sheet

class Sheet:
	index = 0
	cells = []

	def __init__(self, index):
		self.index = index

# Constants
OUTPUT_FILE = 'output.xml'
VIDEO_FILE = 'Evangelion Opening.mp4'
FRAME_INC = 25


# Version
print("Using cv2 version {}".format(cv2.__version__))

# Create temp file
if os.path.exists(OUTPUT_FILE):
	print("Deleting old file '%s'")
	try:
		os.remove(OUTPUT_FILE) 
		print("Done deleting '%s'!")
	except:
		print("Unable to delete '%s'!")
		pass
	

#####################
### Process video ###
#####################

print("Processing video...")
start_processing = time.time()

vidcap = cv2.VideoCapture(VIDEO_FILE)
length = int(vidcap.get(cv2.CAP_PROP_FRAME_COUNT))
count = 0

# Vars
AllSheets = []
sheet_index = 0

while True:
	success, image = vidcap.read()
	if not success:
		break
	
	count += 1
	
	# skip frames
	if count % FRAME_INC != 1 and count != 1:
		continue
	
	print("Frame process: {}/{}".format(count, length))
	
	sheet = Sheet(sheet_index)
	AllSheets.append(sheet)
	
	cells = []
	
	for i in range(image.shape[0]):
		for j in range(image.shape[1]):
			color = '#%02x%02x%02x' % (image[i][j][2], image[i][j][1], image[i][j][0]) #RGB
			c = Cell(i, j, color, sheet_index)
			cells.append(c)
	
	AllSheets[sheet_index].cells = cells
	sheet_index += 1
	
# Done analyzing
end_processing = time.time()
print("Done processing video!")


#######################
### Prepare content ###
#######################

print("Preparing content...")
start_preparing = time.time()

# Content - we need to generate this first so that we can collect unique colors
UniqueColors = []
content = []

for sheet in AllSheets:
	content.append("<Worksheet ss:Name=\"{}\">\n".format("Sheet%d" % sheet.index))
	content.append("<Table>\n")
	
	prev_i = -1
	
	for cell in sheet.cells:
		if cell.color not in UniqueColors:
			UniqueColors.append(cell.color)
	
		if cell.i != prev_i:
			if prev_i != -1:
				content.append("</Row>\n")
			content.append("<Row>\n")
			prev_i = cell.i
		
		style_name = "s%s" % cell.color[1:]
		content.append("<Cell ss:StyleID=\"{}\"></Cell>\n".format(style_name))
	
	content.append("</Row>\n")
	content.append("</Table>\n")
	content.append("</Worksheet>\n")

# Styles
styles = []
styles.append("<Styles>\n")

for i, color in enumerate(UniqueColors):
	style_name = "s%s" % color[1:]
	styles.append("<Style ss:ID=\"{}\">\n".format(style_name))
	styles.append("<Interior ss:Color=\"{}\" ss:Pattern=\"Solid\"/>\n".format(color))
	styles.append("</Style>\n")
	
styles.append("</Styles>\n")

# Done preparing content
end_preparing = time.time()
print("Done preparing content (%d unique colors)!" % len(UniqueColors))



#####################
### Write to file ###
#####################

print("Writing to file '%s'" % (OUTPUT_FILE))
start_write_to_file = time.time()

# Header
header = r'<?xml version="1.0" encoding="UTF-8"?>' + "\n"
header = header + r'<?mso-application progid="Excel.Sheet"?>' + "\n"
header = header + r'<Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet" xmlns:html="http://www.w3.org/TR/REC-html40">' + "\n"

text_file = open(OUTPUT_FILE, "w")
text_file.write(header)

# Styles
for line in styles:
	text_file.write(line)
	
# Content
for line in content:
	text_file.write(line)

# Footer
text_file.write("</Workbook>")
	
# Close file	
text_file.close()

# Done writing to file
end_write_to_file = time.time()
print("Done writing to file!")

# Metrics
processing_time = (end_processing - start_processing)
preparing_time = (end_preparing - start_preparing)
writing_to_file_time = (end_write_to_file - start_write_to_file)
total_time = processing_time + preparing_time + writing_to_file_time
print("=== METRICS ===")
print("Processing video: {}".format(processing_time))
print("Preparing content: {}".format(preparing_time))
print("Writing to file: {}".format(writing_to_file_time))
print("Total time spent: {}".format(total_time))